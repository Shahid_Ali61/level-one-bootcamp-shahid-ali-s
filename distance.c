//WAP to find the distance between two point using 4 functions.
#include<stdio.h>
#include<math.h>
int enter_x1();
int enter_x2();
int enter_y1();
int enter_y2();
float distance(int x1,int x2, int y1, int y2);
void display(float dist);
void main()
{
	int x1,x2,y1,y2;
	float dist;
	x1= enter_x1();
	y1= enter_y1();
	x2= enter_x2();
	y2= enter_y2();
	dist= distance(x1, x2, y1, y2);
	display(dist);
}
int enter_x1()
{
	int x1;
	printf("		First point\n");
	printf("Enter x :");
	scanf("%d",&x1);
	return x1;
}
int enter_y1()
{
	int y1;
	printf("Enter y :");
	scanf("%d",&y1);
	return y1;

}
int enter_x2()
{
	int x2;
	printf("		Second point\n");
	printf("Enter x :");
	scanf("%d",&x2);
	return x2;

}
int enter_y2()
{	
	int y2;
	printf("Enter y :");
	scanf("%d",&y2);
	return y2;
}
float distance(int x1,int x2, int y1, int y2)
{
	float dist = sqrt(pow((x1-x2),2)+pow((y1-y2),2));
	return dist;
}
void display(float dist)
{
	printf("The distance between the 2 given points is : %f",dist);
}