//Write a program to find the sum of n different numbers using 4 functions
#include <stdio.h>
#include <stdlib.h>

struct Point 
{
    int n;
};

int calculate(struct Point a)
{
    int sum=0;
    for (int i = 1; i <= a.n; i++)
    {
        sum = sum + i;
    }
    return sum;
}

int main()
{
    struct Point a;
    printf("Enter an integer number: ");
    scanf ("%d", &a.n);
    printf("Sum of numbers till %d (n) =%d\n",a.n,calculate(a));
    return 0;
}