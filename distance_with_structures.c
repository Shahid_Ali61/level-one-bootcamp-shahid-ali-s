//WAP to find the distance between two points using structures and 4 functions.
#include<stdio.h>
#include<math.h>
typedef struct point
{
    //Point structure with two interger values to store the co-ordinates
	int x;
	int y;
}pt;

pt accept()
{
    //Function that accepts the co-ordinates of the point and returns the point structure.
	pt p;
	printf("Enter x value :");
	scanf("%d",&p.x);
	printf("Enter y value :");
	scanf("%d",&p.y);
return p;
}

float distance(pt p1, pt p2)
{
    //function that accpets two points and calculates the distance between them.
	float dist = sqrt(pow((p1.x-p2.x),2)+pow((p1.y-p2.y),2));
    return dist;
}

void display(float dist)
{
    //A simple function that displays the desired output.
	printf("The distance between the two given points is %f\n",dist);
}

void main()
{
	float dist;
	pt p1, p2;
	p1 = accept();
	p2 = accept();
	dist = distance(p1,p2);
	display(dist);
}