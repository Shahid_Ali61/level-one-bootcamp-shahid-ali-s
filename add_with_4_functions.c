//Write a program to add two user input numbers using 4 functions.

#include<stdio.h>
int entera();
int enterb();
int add(int a, int b);
void display(int sum);
void main()
{
	int a,b,sum;
	//First method to accept the first number
	a = entera();
	
	//Second method to accept the second number
	b = enterb();

	//Third method to calculate the sum
	sum = add(a,b);

	//Fourth method to display the final answer.
	display(sum);
}
int entera()
{
	int a;
	printf("Enter a number:");
	scanf("%d",&a);
	return a;
}
int enterb()
{
	int b;
	printf("Enter a number:");
	scanf("%d",&b);
	return b;
}
int add(int a, int b)
{
	int sum = a+b;
	return sum;
}
void display(int sum)
{
	printf("The sum of the two numbers is %d",sum);
}
