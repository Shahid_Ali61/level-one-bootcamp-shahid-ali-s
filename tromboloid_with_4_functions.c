#include<stdio.h>

int enter_a()
{
 	    float a;
 	    printf("value: ");
 	    scanf("%f",&a);
   	    while(a<0)
   	    {
   	        printf("value cannot be negative, enter again\n");
   	        printf("Enter value: ");
   	        scanf("%f",&a);
   	    }
    	return a;
}

int enter_b()
{
 	    float b;
 	    printf("value: ");
 	    scanf("%f",&b);
   	    while(b<=0)
   	    {
   	        printf("value cannot be 0 or negative, enter again\n");
   	        printf("Enter value: ");
   	        scanf("%f",&b);
   	    }
    	return b;
}

float Volume(float h,float d,float b)
{
    	float vol; 
        vol=((h*d)+d)/(3*b);
    	return vol;
}

void display(float h,float d,float b,float res)
{
    	printf("Height= %f, Depth= %f, Breadth= %f Volume= %f\n",h,d,b,res);
}

int main()
{
    	float h,d,b,res;
    
   	    printf("Enter Height\n");
    	h=enter_a();
    	
    	printf("Enter Depth\n");
    	d=enter_b();
    	
    	printf("Enter Breadth\n");
    	b=enter_b();
    	
    	res=Volume(h,d,b);
  	    display(h,d,b,res);
    	return 0;
}



