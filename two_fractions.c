//WAP to find the sum of two fractions.

#include<stdio.h>

struct Fractn{
  int numo;
  int deno;
};


int input_fn(){
  int a;
  scanf("%d",&a);
  return a;
}

int calc_gcd(int a,int b){
  if(b == 0)
        return a;
    else
        return calc_gcd(b, a%b);
}      
struct Fractn add(struct Fractn f1,struct Fractn f2)
{
  struct Fractn sum;
  sum.numo = (f1.numo*f2.deno+f1.deno*f2.numo);
  sum.deno = f1.deno*f2.deno;
  int gcd = calc_gcd(sum.numo,sum.deno);
  sum.numo = sum.numo/gcd;
  sum.deno = sum.deno/gcd;
  return(sum);
}
void disp(struct Fractn f1,struct Fractn f2, struct Fractn sum)
{
  printf("%d/%d + %d/%d = %d/%d",f1.numo,f1.deno,f2.numo,f2.deno,sum.numo,sum.deno);
}


int main()
{
  struct Fractn f1,f2,sum;
  printf("enter the 1st numerator and denominator:");
  f1.numo = input_fn();
  f1.deno = input_fn();
  printf("enter the 2nd numerator and denominator: ");
  f2.numo = input_fn();
  f2.deno = input_fn();
  sum = add(f1,f2);
  printf(numo,deno,sum);
  return 0;
}
